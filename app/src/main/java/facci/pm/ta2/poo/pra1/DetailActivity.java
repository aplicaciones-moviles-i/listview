package facci.pm.ta2.poo.pra1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    private TextView textViewDescripcion;

    //###################################################################################
    //###################################################################################
    //PREGUNTA 4.2
    //AGREGAR SIGNO EURO CON CODIFICACION UNICODE
    String euro = "\u20ac";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //###################################################################################
        //###################################################################################
        //PREGUNTA 4.2
        //AGREGAR FUNCIONALIDAD DE SCROLL EN EL TEXT VIEW DE DESCRIPCION
        textViewDescripcion = (TextView)findViewById(R.id.TextViewDescripcion);
        textViewDescripcion.setMovementMethod(LinkMovementMethod.getInstance());


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // UOC - BEGIN - CODE6
        //

        //##########################################################################
        //##########################################################################
        //PREGUNTA 4.1
        //ACCEDER AL object_id RECIBIDO COMO PARAMETRO
        //Por medio del método "getIntent" y su método "getStringExtra"
        //accedemos al valor que tenga el nombre "object_id", en nuestro
        //caso es el identificador del producto y lo almacenamos en una
        //variable de tipo String (cadena de texto).
        String object_id = getIntent().getStringExtra("object_id");


        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {

                    //#####################################################################
                    //#####################################################################
                    //PREGUNTA 4.3
                    //ACCEDER A LAS PROPIEDADES DEL object
                    //"object" es un objeto resultado de la consulta individual por el
                    //identificador (ID) del producto a la librería de acceso a datos,
                    //por medio de sus propiedades name, price, description
                    //y image podemos acceder al contenido, para este caso
                    //asignamos los datos a sus respectivos TextViews a través del
                    //método "setText" en el caso de los TextView y "setImageBitmap"
                    //en el caso del ImageView.
                    TextView textViewTitle = (TextView)findViewById(R.id.TextViewTitle);
                    textViewTitle.setText((String) object.get("name"));

                    TextView textViewPrice = (TextView)findViewById(R.id.TextViewPrice);
                    textViewPrice.setText((String) object.get("price") + " " + euro);

                    ImageView  thumbnail=  (ImageView)findViewById(R.id.thumbnail);
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));

                    TextView textViewDescripcion = (TextView)findViewById(R.id.TextViewDescripcion);
                    textViewDescripcion.setText((String) object.get("description"));

                } else {
                    // Error

                }
            }
        });
        // UOC - END - CODE6

    }


    //#######################################################################
    //#######################################################################
    //PREGUNTA 4.4
    //IMPORTANTE
    // Al pulsar el botón back del dispositivo Android se debe volver a
    // la pantalla de índice, y esto lo hice utilizando un Intent y
    //sobre escribiendo el método "onBackPressed"
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent;
        intent = new Intent(DetailActivity.this, ResultsActivity.class);
        startActivity(intent);
        finish();

    }
}
